package com.challenge.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.api.dto.ChallengeAPIRequest;
import com.challenge.api.dto.ChallengeAPIResponse;
import com.challenge.api.properties.ChallengeAPIProperties;
import com.challenge.api.service.ChallengeTopSecretService;
import com.challenge.api.service.ChallengeTopSecretSplitService;
import com.challenge.api.utils.Utils;

@RestController
public class ChallengeAPIController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChallengeAPIController.class);

	@Autowired
	ChallengeAPIProperties properties;

	@Autowired
	ChallengeTopSecretService topSecretservice;

	@Autowired
	ChallengeTopSecretSplitService topSecretSplitservice;

	@GetMapping("/saludo")
	public ResponseEntity<Object> saludo() {

		String mensaje = "Saludando desde Technical Chalenge API";
		return new ResponseEntity<Object>(mensaje, HttpStatus.OK);
	}

	/**
	 * TopSecret
	 * 
	 * @param request
	 * @return
	 */
	@PostMapping("/topsecret")
	public ResponseEntity<ChallengeAPIResponse> topSecret(@RequestBody ChallengeAPIRequest request) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_TOP_SECRET);

		ResponseEntity<ChallengeAPIResponse> wsResponse = null;
		ChallengeAPIResponse dtoResp = null;

		try {

			LOGGER.debug(Utils.LOG_CALLING_GET_LOCATION_METHOD);
			dtoResp = topSecretservice.getLocation(request);

		} catch (Exception ex) {
			LOGGER.error(Utils.LOG_EXCEPTION, ex);
			dtoResp = new ChallengeAPIResponse();
			dtoResp.setResponseCode(properties.getErrorCode());
			dtoResp.setResponseMessage(properties.getExceptionMsg());

		} finally {
			LOGGER.debug(Utils.LOG_BUILDING_RESPONSE_METHOD, Utils.METHOD_TOP_SECRET);
			wsResponse = new ResponseEntity<>(dtoResp, HttpStatus.OK);

			LOGGER.info(Utils.LOG_TOP_SECRET_METHOD, dtoResp.getResponseCode(), dtoResp.getResponseMessage());
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_TOP_SECRET);
		return wsResponse;
	}

	/**
	 * Get TopSecretSplit
	 * 
	 * @param request
	 * @param satelliteName
	 * @return
	 */
	@PostMapping("/topsecret_split/{satellite_name}")
	public ResponseEntity<ChallengeAPIResponse> getTopSecretSplit(@RequestBody ChallengeAPIRequest request,
			@PathVariable(name = "satellite_name", required = true) String satelliteName) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_TOP_SECRET_SPLIT);

		ResponseEntity<ChallengeAPIResponse> wsResponse = null;
		ChallengeAPIResponse dtoResp = null;

		try {

			LOGGER.debug(Utils.LOG_CALLING_TOP_SECRET_SPLIT);
			dtoResp = topSecretSplitservice.getTopSecretSplit(request, satelliteName);

		} catch (Exception ex) {
			LOGGER.error(Utils.LOG_EXCEPTION, ex);
			dtoResp = new ChallengeAPIResponse();
			dtoResp.setResponseCode(properties.getErrorCode());
			dtoResp.setResponseMessage(properties.getExceptionMsg());

		} finally {
			LOGGER.debug(Utils.LOG_BUILDING_RESPONSE_METHOD, Utils.METHOD_TOP_SECRET_SPLIT);
			wsResponse = new ResponseEntity<>(dtoResp, HttpStatus.OK);

			LOGGER.info(Utils.LOG_TOP_SECRET_SPLIT_METHOD, dtoResp.getResponseCode(), dtoResp.getResponseMessage());
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_TOP_SECRET_SPLIT);
		return wsResponse;
	}

}
