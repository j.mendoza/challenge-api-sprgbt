package com.challenge.api.utils;

import java.util.Objects;

public class Utils {

	public static final String INSIDE_METHOD = "Inside method: {}";
	public static final String FINISH_METHOD = "Finish method: {}";
	public static final String LOG_EXCEPTION = "EXCEPTION: ";

	public static final String KENOBI = "KENOBI";
	public static final String SKYWALKER = "SKYWALKER";
	public static final String SATO = "SATO";

	public static final String METHOD_TOP_SECRET = "TOP_SECRET";
	public static final String METHOD_TOP_SECRET_SPLIT = "TOP_SECRET_SPLIT";
	public static final String METHOD_GET_LOCATION = "GET_LOCATION";
	public static final String METHOD_GET_TOP_SECRET_SPLIT = "GET_TOP_SECRET_SPLIT";
	public static final String METHOD_PREVIOUS_VALIDATIONS_GET_LOCATION_REQUEST = "PREVIOUS_VALIDATIONS_GET_LOCATION_REQUEST";
	public static final String METHOD_PREVIOUS_VALIDATIONS_TOP_SECRET_SPLIT_REQUEST = "PREVIOUS_VALIDATIONS_TOP_SECRET_SPLIT_REQUEST";
	public static final String METHOD_PROCESS_TOP_SECRET_SPLIT_REQUEST = "PROCESS__TOP_SECRET_SPLIT_REQUEST";
	public static final String METHOD_CHECK_DISTANCES_GET_LOCATION_REQUEST = "CHECK_DISTANCES_GET_LOCATION_REQUEST";
	public static final String METHOD_BUILDING_RESPONSE = "BUILDING_RESPONSE";
	public static final String METHOD_CHECK_SATELLITE_INFO = "CHECK_SATELLITE_INFO";
	public static final String METHOD_CHECK_SATELLITE_NAME = "CHECK_SATELLITE_NAME";
	public static final String METHOD_PROCESS_TOP_SECRET = "PROCESS_TOP_SECRET";
	public static final String METHOD_CALCULATE_GET_LOCATION = "CALCULATE_GET_LOCATION";
	public static final String METHOD_CULATE_COORDINATES_SPACESHIP_TO_EACH_SATELLITE = "CULATE_COORDINATES_SPACESHIP_TO_EACH_SATELLITE";
	public static final String METHOD_COMPARE_COORDINATES_SPACESHIP_OBTAINED = "COMPARE_COORDINATES_SPACESHIP_OBTAINED";
	public static final String METHOD_GET_MESSAGE = "GET_MESSAGE";
	public static final String METHOD_GET_POSITION_EACH_SATELLITE = "GET_POSITION_EACH_SATELLITE";

	public static final String LOG_CALLING_GET_LOCATION_METHOD = "CALLING_GET_LOCATION_METHOD";
	public static final String LOG_CALLING_TOP_SECRET_SPLIT = "CALLING_TOP_SECRET_SPLIT";
	public static final String LOG_BUILDING_RESPONSE_METHOD = "BUILDING_RESPONSE_METHOD: {}";
	public static final String LOG_TOP_SECRET_METHOD = "TOP_SECRET_RESPONSE_METHOD: [ responseCode: {} - responseMessage: {} ]";
	public static final String LOG_TOP_SECRET_SPLIT_METHOD = "TOP_SECRET_SPLIT_RESPONSE_METHOD: [ responseCode: {} - responseMessage: {} ]";
	public static final String LOG_CHECK_GET_LOCATION = "CHECK_GET_LOCATION";
	public static final String LOG_PROCESS_GET_LOCATION = "PROCESS_GET_LOCATION";
	public static final String LOG_PROCESS_TOP_SECRET_SPLIT = "PROCESS_TOP_SECRET_SPLIT";
	public static final String LOG_CHECK_TOP_SECRET_SPLIT = "CHECK_TOP_SECRET_SPLIT";
	public static final String LOG_BAD_REQUEST_DISTANCES_GET_LOCATION = "BAD_REQUEST_DISTANCES_GET_LOCATION";
	public static final String LOG_BAD_REQUEST_INCOMPLETE_NUMBER_OF_SATELLITE_DISTANCES_GET_LOCATION = "BAD_REQUEST_INCOMPLETE_NUMBER_OF_SATELLITE_DISTANCES_GET_LOCATION";
	public static final String LOG_ERROR_METHOD = "ERROR_METHOD: [ {} ] - [ responseCode: {} - responseMessage: {} ]";
	public static final String LOG_BAD_REQUEST_NULL_SATELLITE_NAME = "BAD_REQUEST_NULL_SATELLITE_NAME";
	public static final String LOG_BAD_REQUEST_BAD_SATELLITE_NAME = "BAD_REQUEST_BAD_SATELLITE_NAME";
	public static final String LOG_BAD_REQUEST_BAD_SATELLITE_DISTANCE = "BAD_REQUEST_BAD_SATELLITE_DISTANCE";
	public static final String LOG_BAD_REQUEST_BAD_SATELLITE_MESSAGE = "BAD_REQUEST_BAD_SATELLITE_MESSAGE";
	public static final String LOG_CULATING_COORDINATES_SPACESHIP_TO_SATELLITE = "CULATING_COORDINATES_SPACESHIP_TO_SATELLITE_{}";
	public static final String LOG_COORDINATES_FOUND_RESPECT_TO_SATELLITE = "COORDINATES_FOUND_RESPECT_TO_SATELLITE_{}: (X,Y) => ({},{})";
	public static final String LOG_ERROR_GETTING_POSITION_SPACESHIP = "ERROR_GETTING_POSITION_SPACESHIP";
	public static final String LOG_ERROR_POSITION_SPACESHIP_NOT_FOUND = "POSITION_SPACESHIP_NOT_FOUND";
	public static final String LOG_POSITION_SPACESHIP_WAS_FOUND = "POSITION_SPACESHIP_WAS_FOUND";

	/**
	 * Default Constructor
	 */
	private Utils() {

	}

	/**
	 * equals
	 * 
	 * @param <TYPE>
	 * @param aObj
	 * @param bObj
	 * @param cObj
	 * @return
	 */
	public static <TYPE> boolean equals(TYPE aObj, TYPE bObj, TYPE cObj) {
		return Objects.equals(aObj, bObj) && Objects.equals(bObj, cObj) && Objects.equals(cObj, aObj);
	}

}
