package com.challenge.api.service;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.api.dto.ChallengeAPIRequest;
import com.challenge.api.dto.ChallengeAPIResponse;
import com.challenge.api.dto.Position;
import com.challenge.api.dto.PreRequerimentsDto;
import com.challenge.api.dto.Satellite;
import com.challenge.api.properties.ChallengeAPIProperties;
import com.challenge.api.utils.Utils;

@Service
public class ChallengeTopSecretService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChallengeTopSecretService.class);

	@Autowired
	ChallengeAPIProperties properties;

	/**
	 * Calculate the position of the spaceship given the distance between the
	 * spaceship and the satellites
	 * 
	 * @param distances
	 * @return Spaceship position(X,Y)
	 */
	public ChallengeAPIResponse getLocation(ChallengeAPIRequest request) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_GET_LOCATION);

		ChallengeAPIResponse dtoResp = null;

		LOGGER.debug(Utils.LOG_CHECK_GET_LOCATION);
		dtoResp = previousValidationsGetLocationRequest(request.getSatellites());

		if (dtoResp == null) {

			PreRequerimentsDto preRequerimentsDto = new PreRequerimentsDto();

			LOGGER.debug(Utils.LOG_PROCESS_GET_LOCATION);
			dtoResp = processTopSecret(request.getSatellites(), preRequerimentsDto);
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_GET_LOCATION);
		return dtoResp;
	}

	/*
	 * ******************************************************************* PRIVATE
	 * METHODS *******************************************************************
	 */

	/**
	 * Used to check the information received in the request
	 * 
	 * @param distances
	 * @return ChallengeAPIResponse
	 */
	private ChallengeAPIResponse previousValidationsGetLocationRequest(Satellite[] satellites) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_PREVIOUS_VALIDATIONS_GET_LOCATION_REQUEST);

		ChallengeAPIResponse dtoResp = null;

		if ((dtoResp = checkDistancesGetLocationRequest(satellites)) != null) {
			LOGGER.warn(Utils.LOG_ERROR_METHOD, Utils.METHOD_CHECK_DISTANCES_GET_LOCATION_REQUEST,
					dtoResp.getResponseCode(), dtoResp.getResponseMessage());
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_PREVIOUS_VALIDATIONS_GET_LOCATION_REQUEST);
		return dtoResp;
	}

	/**
	 * Used to check the information distances received in the request
	 * 
	 * @param distances
	 * @return
	 */
	private ChallengeAPIResponse checkDistancesGetLocationRequest(Satellite[] satellites) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_CHECK_DISTANCES_GET_LOCATION_REQUEST);

		ChallengeAPIResponse dtoResp = null;

		if (ArrayUtils.isEmpty(satellites)) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_DISTANCES_GET_LOCATION);
			dtoResp = buildingResponse(properties.getErrorCode(), properties.getBadDistancesMsg());

		} else if (!properties.getNumberSatellites().equals(satellites.length)) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_INCOMPLETE_NUMBER_OF_SATELLITE_DISTANCES_GET_LOCATION);
			dtoResp = buildingResponse(properties.getErrorCode(), properties.getBadNumberDistancesMsg());

		} else if ((dtoResp = checkSatelliteInfo(satellites[0])) != null) {
			LOGGER.warn(Utils.LOG_ERROR_METHOD, Utils.METHOD_CHECK_SATELLITE_INFO, dtoResp.getResponseCode(),
					dtoResp.getResponseMessage());

		} else if ((dtoResp = checkSatelliteInfo(satellites[1])) != null) {
			LOGGER.warn(Utils.LOG_ERROR_METHOD, Utils.METHOD_CHECK_SATELLITE_INFO, dtoResp.getResponseCode(),
					dtoResp.getResponseMessage());

		} else if ((dtoResp = checkSatelliteInfo(satellites[2])) != null) {
			LOGGER.warn(Utils.LOG_ERROR_METHOD, Utils.METHOD_CHECK_SATELLITE_INFO, dtoResp.getResponseCode(),
					dtoResp.getResponseMessage());
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_CHECK_DISTANCES_GET_LOCATION_REQUEST);
		return dtoResp;
	}

	/**
	 * Used to check the information each Satellite received in the request
	 * 
	 * @param satellite
	 * @return
	 */
	private ChallengeAPIResponse checkSatelliteInfo(Satellite satellite) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_CHECK_SATELLITE_INFO);

		ChallengeAPIResponse dtoResp = null;

		if (StringUtils.isBlank(satellite.getName())) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_NULL_SATELLITE_NAME);
			dtoResp = buildingResponse(properties.getErrorCode(), properties.getBadSatelliteNameMsg());

		} else if (!checkSatelliteName(satellite.getName())) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_BAD_SATELLITE_NAME);
			dtoResp = buildingResponse(properties.getErrorCode(), properties.getBadSatelliteNameMsg());

		} else if (satellite.getDistance() <= 0.0f) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_BAD_SATELLITE_DISTANCE);
			dtoResp = buildingResponse(properties.getErrorCode(), properties.getBadSatelliteDistanceMsg());

		} else if (ArrayUtils.isEmpty(satellite.getMessage())) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_BAD_SATELLITE_MESSAGE);
			dtoResp = buildingResponse(properties.getErrorCode(), properties.getBadSatelliteMessageMsg());
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_CHECK_SATELLITE_INFO);
		return dtoResp;
	}

	/**
	 * Used to check the information aboute the name of each Satellite received in
	 * the request
	 * 
	 * @param nameSatellite
	 * @return
	 */
	private boolean checkSatelliteName(String nameSatellite) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_CHECK_SATELLITE_NAME);

		boolean dtoResp = false;

		String nameSatellites[] = properties.getNameSatellites().split(properties.getSplitRegex());

		if (!ArrayUtils.isEmpty(nameSatellites)) {

			for (String name : nameSatellites) {
				if (name.contentEquals(nameSatellite.toUpperCase())) {
					dtoResp = true;
				}
			}
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_CHECK_SATELLITE_NAME);
		return dtoResp;
	}

	/**
	 * Used to process the information about of Satellites received in the request
	 * 
	 * @param satellites
	 * @param PreRequerimentsDto
	 * @return
	 */
	private ChallengeAPIResponse processTopSecret(Satellite[] satellites, PreRequerimentsDto preRequerimentsDto) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_PROCESS_TOP_SECRET);

		ChallengeAPIResponse dtoResp = null;

		if ((dtoResp = processCalculateLocation(satellites, preRequerimentsDto)) != null) {
			LOGGER.warn(Utils.LOG_ERROR_METHOD, Utils.METHOD_CALCULATE_GET_LOCATION, dtoResp.getResponseCode(),
					dtoResp.getResponseMessage());

		} else if ((dtoResp = compareCoordinatesSpaceshipObtained(satellites, preRequerimentsDto)) != null) {
			LOGGER.warn(Utils.LOG_ERROR_METHOD, Utils.METHOD_COMPARE_COORDINATES_SPACESHIP_OBTAINED,
					dtoResp.getResponseCode(), dtoResp.getResponseMessage());
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_PROCESS_TOP_SECRET);
		return dtoResp;
	}

	/**
	 * Used to calculate the information about of Satellites received in the request
	 * 
	 * @param satellites
	 * @param PreRequerimentsDto
	 * @return
	 */
	private ChallengeAPIResponse processCalculateLocation(Satellite[] satellites,
			PreRequerimentsDto PreRequerimentsDto) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_CALCULATE_GET_LOCATION);

		ChallengeAPIResponse dtoResp = null;
		Position positionDto = null;

		try {

			Position positionKenobi = new Position(properties.getKenobiX(), properties.getKenobiY());
			Position positionSkywalker = new Position(properties.getSkywalkerX(), properties.getSkywalkerY());
			Position positionSato = new Position(properties.getSatoX(), properties.getSatoY());

			LOGGER.debug(Utils.LOG_CULATING_COORDINATES_SPACESHIP_TO_SATELLITE, satellites[0].getName().toUpperCase());
			positionDto = culateCoordinatesSpaceshipToEachSatellite(positionKenobi, satellites[0].getDistance(),
					positionSkywalker);
			LOGGER.info(Utils.LOG_COORDINATES_FOUND_RESPECT_TO_SATELLITE, satellites[0].getName().toUpperCase(),
					positionDto.getX(), positionDto.getY());
			PreRequerimentsDto.getMapCoordinatesSpaceship().put(satellites[0].getName().toUpperCase(), positionDto);

			LOGGER.debug(Utils.LOG_CULATING_COORDINATES_SPACESHIP_TO_SATELLITE, satellites[1].getName().toUpperCase());
			positionDto = culateCoordinatesSpaceshipToEachSatellite(positionSkywalker, satellites[1].getDistance(),
					positionSato);
			LOGGER.info(Utils.LOG_COORDINATES_FOUND_RESPECT_TO_SATELLITE, satellites[1].getName().toUpperCase(),
					positionDto.getX(), positionDto.getY());
			PreRequerimentsDto.getMapCoordinatesSpaceship().put(satellites[1].getName().toUpperCase(), positionDto);

			LOGGER.debug(Utils.LOG_CULATING_COORDINATES_SPACESHIP_TO_SATELLITE, satellites[2].getName().toUpperCase());
			positionDto = culateCoordinatesSpaceshipToEachSatellite(positionSato, satellites[2].getDistance(),
					positionKenobi);
			LOGGER.info(Utils.LOG_COORDINATES_FOUND_RESPECT_TO_SATELLITE, satellites[2].getName().toUpperCase(),
					positionDto.getX(), positionDto.getY());
			PreRequerimentsDto.getMapCoordinatesSpaceship().put(satellites[2].getName().toUpperCase(), positionDto);

		} catch (Exception ex) {
			LOGGER.error(Utils.LOG_EXCEPTION, ex);
			dtoResp = new ChallengeAPIResponse();
			dtoResp.setResponseCode(properties.getErrorCode());
			dtoResp.setResponseMessage(properties.getExceptionMsg());
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_CALCULATE_GET_LOCATION);
		return dtoResp;
	}

	/**
	 * Use to calculate coordinates of Spaceship to each Satellite
	 * 
	 * @param position1
	 * @param distance
	 * @param position2
	 * @return Position(X,Y)
	 */
	private Position culateCoordinatesSpaceshipToEachSatellite(Position position1, float distance, Position position2) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_CULATE_COORDINATES_SPACESHIP_TO_EACH_SATELLITE);

		Position positionDto = null;

		// Position 1 coordinates
		double x1 = Double.parseDouble(new Float(position1.getX()).toString());
		double y1 = Double.parseDouble(new Float(position1.getY()).toString());

		// Position 2 coordinates
		double x2 = Double.parseDouble(new Float(position2.getX()).toString());
		double y2 = Double.parseDouble(new Float(position2.getY()).toString());

		// Calculate the distance between the two points: Point1 & Point2
		double distanceBetweenPoint1ToPoint2 = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
		LOGGER.debug("distance between Point1 to Point2: {}", distanceBetweenPoint1ToPoint2);

		// Getting reason
		double reason = distance / distanceBetweenPoint1ToPoint2;
		LOGGER.debug("reason: {}", reason);

		// Find the coordinates of the Spaceship
		double x = (1 - reason) * x1 + reason * x2;
		float cordX = (float) x;

		double y = (1 - reason) * y1 + reason * y2;
		float cordY = (float) y;

		positionDto = new Position(cordX, cordY);

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_CULATE_COORDINATES_SPACESHIP_TO_EACH_SATELLITE);
		return positionDto;
	}

	/**
	 * Compare Coordinates Spaceship Obtained
	 * 
	 * @param PreRequerimentsDto
	 * @return
	 */
	private ChallengeAPIResponse compareCoordinatesSpaceshipObtained(Satellite[] satellites,
			PreRequerimentsDto preRequerimentsDto) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_COMPARE_COORDINATES_SPACESHIP_OBTAINED);

		ChallengeAPIResponse dtoResp = null;

		Position positionSpaceShipKenobi = preRequerimentsDto.getMapCoordinatesSpaceship()
				.get(satellites[0].getName().toUpperCase());
		Position positionSpaceShipSkywalker = preRequerimentsDto.getMapCoordinatesSpaceship()
				.get(satellites[1].getName().toUpperCase());
		Position positionSpaceShipSato = preRequerimentsDto.getMapCoordinatesSpaceship()
				.get(satellites[2].getName().toUpperCase());

		if (positionSpaceShipKenobi == null || positionSpaceShipSkywalker == null || positionSpaceShipSato == null) {
			LOGGER.warn(Utils.LOG_ERROR_GETTING_POSITION_SPACESHIP);
			dtoResp = buildingResponse(properties.getErrorCode(), properties.getErrorGettingPositionsMsg());

		} else if (!Utils.equals(positionSpaceShipKenobi, positionSpaceShipSkywalker, positionSpaceShipSato)) {
			LOGGER.warn(Utils.LOG_ERROR_POSITION_SPACESHIP_NOT_FOUND);
			dtoResp = buildingResponse(properties.getErrorCode(), properties.getErrorPositionNotFoundMsg());

		} else {
			LOGGER.info(Utils.LOG_POSITION_SPACESHIP_WAS_FOUND);
			dtoResp = buildingResponse(properties.getSuccessCode(), properties.getSuccessMsg());
			dtoResp.setPosition(positionSpaceShipKenobi);
			dtoResp.setMessage(getMessage(satellites));
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_COMPARE_COORDINATES_SPACESHIP_OBTAINED);
		return dtoResp;
	}

	/**
	 * Get Messages
	 * 
	 * @param satellites
	 * @return
	 */
	private String getMessage(Satellite[] satellites) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_GET_MESSAGE);

		String message = "";

		for (String msgPart1 : satellites[0].getMessage()) {
			if (StringUtils.isNotBlank(msgPart1) && !message.contains(msgPart1)) {
				message += msgPart1.concat(" ");
			}
		}

		for (String msgPart2 : satellites[1].getMessage()) {
			if (StringUtils.isNotBlank(msgPart2) && !message.contains(msgPart2)) {
				message += msgPart2.concat(" ");
			}
		}

		for (String msgPart3 : satellites[2].getMessage()) {
			if (StringUtils.isNotBlank(msgPart3) && !message.contains(msgPart3)) {
				message += msgPart3.concat(" ");
			}
		}

		LOGGER.info("Mensaje: {}", message);

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_GET_MESSAGE);
		return message;
	}

	/**
	 * Used to construct the response
	 * 
	 * @param responseCode
	 * @param responseMessage
	 * @return ChallengeAPIResponse
	 */
	private ChallengeAPIResponse buildingResponse(Integer responseCode, String responseMessage) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_BUILDING_RESPONSE);

		ChallengeAPIResponse dtoResp = new ChallengeAPIResponse();
		dtoResp.setResponseCode(responseCode);
		dtoResp.setResponseMessage(responseMessage);

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_BUILDING_RESPONSE);
		return dtoResp;
	}

}
