package com.challenge.api.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.api.dto.ChallengeAPIRequest;
import com.challenge.api.dto.ChallengeAPIResponse;
import com.challenge.api.dto.Position;
import com.challenge.api.dto.Satellite;
import com.challenge.api.properties.ChallengeAPIProperties;
import com.challenge.api.utils.Utils;

@Service
public class ChallengeTopSecretSplitService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChallengeTopSecretSplitService.class);

	@Autowired
	ChallengeAPIProperties properties;

	/*
	 * Map to store the messages received at each POST
	 */
	Map<String, String> mapSatelliteMessages = new HashMap<>();

	/**
	 * Get To Secret Split
	 * 
	 * @param request
	 * @return
	 */
	public ChallengeAPIResponse getTopSecretSplit(ChallengeAPIRequest request, String satelliteName) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_GET_TOP_SECRET_SPLIT);

		ChallengeAPIResponse dtoResp = null;

		Satellite satellite = new Satellite();
		satellite.setName(satelliteName);
		satellite.setDistance(request.getDistance());
		satellite.setMessage(request.getMessage());

		LOGGER.debug(Utils.LOG_CHECK_TOP_SECRET_SPLIT);
		dtoResp = previousValidationsTopSecretSprlit(satellite);

		if (dtoResp == null) {

			LOGGER.debug(Utils.LOG_PROCESS_TOP_SECRET_SPLIT);
			dtoResp = processTopSecretSplit(satellite);
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_GET_TOP_SECRET_SPLIT);
		return dtoResp;
	}

	/*
	 * ******************************************************************* PRIVATE
	 * METHODS *******************************************************************
	 */

	/**
	 * Used to check the information received in the request
	 * 
	 * @param satellite
	 * @return ChallengeAPIResponse
	 */
	private ChallengeAPIResponse previousValidationsTopSecretSprlit(Satellite satellite) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_PREVIOUS_VALIDATIONS_TOP_SECRET_SPLIT_REQUEST);

		ChallengeAPIResponse dtoResp = null;

		if ((dtoResp = checkSatelliteInfo(satellite)) != null) {
			LOGGER.warn(Utils.LOG_ERROR_METHOD, Utils.METHOD_CHECK_DISTANCES_GET_LOCATION_REQUEST,
					dtoResp.getResponseCode(), dtoResp.getResponseMessage());
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_PREVIOUS_VALIDATIONS_TOP_SECRET_SPLIT_REQUEST);
		return dtoResp;
	}

	/**
	 * Used to check the information each Satellite received in the request
	 * 
	 * @param satellite
	 * @return
	 */
	private ChallengeAPIResponse checkSatelliteInfo(Satellite satellite) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_CHECK_SATELLITE_INFO);

		ChallengeAPIResponse dtoResp = null;

		if (StringUtils.isBlank(satellite.getName())) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_NULL_SATELLITE_NAME);
			dtoResp = buildingResponse(properties.getErrorCode(), properties.getBadSatelliteNameMsg());

		} else if (!checkSatelliteName(satellite.getName())) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_BAD_SATELLITE_NAME);
			dtoResp = buildingResponse(properties.getErrorCode(), properties.getBadSatelliteNameMsg());

		} else if (satellite.getDistance() <= 0.0f) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_BAD_SATELLITE_DISTANCE);
			dtoResp = buildingResponse(properties.getErrorCode(), properties.getBadSatelliteDistanceMsg());

		} else if (ArrayUtils.isEmpty(satellite.getMessage())) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_BAD_SATELLITE_MESSAGE);
			dtoResp = buildingResponse(properties.getErrorCode(), properties.getBadSatelliteMessageMsg());
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_CHECK_SATELLITE_INFO);
		return dtoResp;
	}

	/**
	 * Used to check the information aboute the name of each Satellite received in
	 * the request
	 * 
	 * @param nameSatellite
	 * @return
	 */
	private boolean checkSatelliteName(String nameSatellite) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_CHECK_SATELLITE_NAME);

		boolean dtoResp = false;

		String nameSatellites[] = properties.getNameSatellites().split(properties.getSplitRegex());

		if (!ArrayUtils.isEmpty(nameSatellites)) {

			for (String name : nameSatellites) {
				if (name.contentEquals(nameSatellite.toUpperCase())) {
					dtoResp = true;
				}
			}
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_CHECK_SATELLITE_NAME);
		return dtoResp;
	}

	/**
	 * Process Top SecretSplit
	 * 
	 * @param satellite
	 * @return
	 */
	private ChallengeAPIResponse processTopSecretSplit(Satellite satellite) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_PROCESS_TOP_SECRET_SPLIT_REQUEST);

		ChallengeAPIResponse dtoResp = null;

		String message = StringUtils.isBlank(mapSatelliteMessages.get(satellite.getName().toUpperCase())) ? ""
				: mapSatelliteMessages.get(satellite.getName().toUpperCase());

		for (String msgPart : satellite.getMessage()) {
			if (StringUtils.isNotBlank(msgPart) && !message.contains(msgPart)) {
				message += msgPart.concat(" ");
			}
		}

		mapSatelliteMessages.put(satellite.getName().toUpperCase(), message);

		Position positionDto = getPositionEachSatellite(satellite);

		LOGGER.info(Utils.LOG_POSITION_SPACESHIP_WAS_FOUND);
		dtoResp = buildingResponse(properties.getSuccessCode(), properties.getSuccessMsg());
		dtoResp.setPosition(positionDto);
		dtoResp.setMessage(message);

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_PROCESS_TOP_SECRET_SPLIT_REQUEST);
		return dtoResp;
	}

	/**
	 * Get Position each Satellite
	 * 
	 * @param satellite
	 * @return
	 */
	private Position getPositionEachSatellite(Satellite satellite) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_GET_POSITION_EACH_SATELLITE);

		Position positionDto = null;

		switch (satellite.getName().toUpperCase()) {
		case Utils.KENOBI:
			positionDto = new Position(properties.getKenobiX(), properties.getKenobiY());
			break;
		case Utils.SKYWALKER:
			positionDto = new Position(properties.getSkywalkerX(), properties.getSkywalkerY());
			break;

		case Utils.SATO:
		default:
			positionDto = new Position(properties.getSatoX(), properties.getSatoY());
			break;
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_GET_POSITION_EACH_SATELLITE);
		return positionDto;
	}

	/**
	 * Used to construct the response
	 * 
	 * @param responseCode
	 * @param responseMessage
	 * @return ChallengeAPIResponse
	 */
	private ChallengeAPIResponse buildingResponse(Integer responseCode, String responseMessage) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_BUILDING_RESPONSE);

		ChallengeAPIResponse dtoResp = new ChallengeAPIResponse();
		dtoResp.setResponseCode(responseCode);
		dtoResp.setResponseMessage(responseMessage);

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_BUILDING_RESPONSE);
		return dtoResp;
	}
}
