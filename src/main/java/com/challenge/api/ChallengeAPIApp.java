package com.challenge.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChallengeAPIApp {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChallengeAPIApp.class);

	public static void main(String[] args) {
		LOGGER.info("*** Starting Challenge API REST ***");
		SpringApplication.run(ChallengeAPIApp.class, args);
	}

}
