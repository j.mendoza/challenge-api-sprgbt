package com.challenge.api.dto;

public class ChallengeAPIRequest {

	// Refers to the distances from each satellite to the spaceship
	private Satellite[] satellites;

	// Refers to the distance from the satellite to the spaceship
	private float distance;

	// Refers to the message received from the spaceship by the satellite
	private String[] message;

	/**
	 * Getter & Setter
	 */

	/**
	 * @return the satellites
	 */
	public Satellite[] getSatellites() {
		return satellites;
	}

	/**
	 * @param satellites the satellites to set
	 */
	public void setSatellites(Satellite[] satellites) {
		this.satellites = satellites;
	}

	/**
	 * @return the distance
	 */
	public float getDistance() {
		return distance;
	}

	/**
	 * @param distance the distance to set
	 */
	public void setDistance(float distance) {
		this.distance = distance;
	}

	/**
	 * @return the message
	 */
	public String[] getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String[] message) {
		this.message = message;
	}

}
