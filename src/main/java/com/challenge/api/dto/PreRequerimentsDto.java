package com.challenge.api.dto;

import java.util.HashMap;
import java.util.Map;

public class PreRequerimentsDto {

	private Map<String, Position> mapCoordinatesSpaceship = new HashMap<>();

	/*
	 * Getter & Setter
	 */

	/**
	 * @return the mapCoordinatesSpaceship
	 */
	public Map<String, Position> getMapCoordinatesSpaceship() {
		return mapCoordinatesSpaceship;
	}

	/**
	 * @param mapCoordinatesSpaceship the mapCoordinatesSpaceship to set
	 */
	public void setMapCoordinatesSpaceship(Map<String, Position> mapCoordinatesSpaceship) {
		this.mapCoordinatesSpaceship = mapCoordinatesSpaceship;
	}

}
