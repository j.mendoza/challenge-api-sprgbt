package com.challenge.api.dto;

public class Position {

	// Refers to the coordinate on the X axis
	private float x;

	// Refers to the coordinate on the Y axis
	private float y;

	/*
	 * Default Constructor
	 */
	public Position() {

	}

	/*
	 * Parameters Constructor
	 */
	public Position(float x, float y) {
		this.x = x;
		this.y = y;
	}

	/*
	 * Getter & Setter
	 */

	/**
	 * @return the x
	 */
	public float getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public float getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(float y) {
		this.y = y;
	}

}
