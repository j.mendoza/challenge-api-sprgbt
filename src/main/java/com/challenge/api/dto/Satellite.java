package com.challenge.api.dto;

public class Satellite {

	// Refers to the name of the satellite
	private String name;

	// Refers to the distance from the satellite to the spaceship
	private float distance;

	// Refers to the message received from the spaceship by the satellite
	private String[] message;

	/*
	 * Getter & Setter
	 */

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the distance
	 */
	public float getDistance() {
		return distance;
	}

	/**
	 * @param distance the distance to set
	 */
	public void setDistance(float distance) {
		this.distance = distance;
	}

	/**
	 * @return the message
	 */
	public String[] getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String[] message) {
		this.message = message;
	}

}
